package com.example.kafka.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer {
    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(Consumer.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @KafkaListener(topics = "${topic.name}")
    public void listen(String message) {
        log.info("Received: " + message);
        String sql = "INSERT INTO kafka (message) VALUES ('" + message + "')";
        jdbcTemplate.update(sql);
    }
}